/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

let createDirFile = function (){
const fs = require("fs");

fs.mkdir("directory", (err) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Directory created");

    fs.writeFile(`directory/file1.json`, JSON.stringify([1,2,3,4]), (err) => {
      if (err) console.log(err.message);
      else console.log("file1 created successfully");
    });

    fs.writeFile('directory/file2.json',JSON.stringify("Hello world"),(err)=>{
        if(err) console.log(err.message)
        else console.log ('file2 created successfully')
    })


  }
});
}

module.exports = createDirFile