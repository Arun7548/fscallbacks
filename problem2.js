/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
let fsFile = function () {
  //read the li[sum.txt file data in utf-8 formate.
  fs.readFile("lipsum.txt", "utf-8", (err, data) => {
    if (err) console.log(err.message);
    else {
      //mofify data into uppercase and save in file.
      fs.writeFile("upperFile.txt", data.toUpperCase(), (err) => {
        if (err) console.log(err.message);
        else {
          console.log("upperfile created successfully..!!");

          //save the file name in to filenames.txt file.
          fs.writeFile("filenames.txt", "upperFile.txt", (err) => {
            if (err) console.log(err.message);
            else {
              console.log("file created successfully..!!");
              //read the data of upperfile.txt
              fs.readFile("upperFile.txt", "utf-8", (err, data) => {
                if (err) console.log(err.message);
                else {
                  //split each sentences. and save in file.
                  fs.writeFile(
                    "splitFile.txt",
                    JSON.stringify(data.toLowerCase().split(".")),
                    (err) => {
                      if (err) console.log(err.message);
                      else {
                        console.log("split file created successfully..!!");

                        //appending splitfilename into filenames.txt
                        fs.appendFile(
                          "filenames.txt",
                          "\nsplitFile.txt",
                          (err) => {
                            if (err) console.log(err.message);
                            else {
                              console.log(
                                "split file name appended in filenames.txt"
                              );

                              fs.readFile(
                                "splitFile.txt",
                                "utf-8",
                                (err, data) => {
                                  if (err) console.log(err.message);
                                  else {
                                    const sortData = JSON.parse(data).sort();
                                    
                                    fs.writeFile(
                                      "sortFile.txt",
                                     JSON.stringify( sortData),
                                      (err) => {
                                        if (err) console.log(err.message);
                                        else {
                                          console.log("sortFile created..!!");

                                          fs.appendFile("filenames.txt","\nsortFile.txt",(err)=>{
                                            if(err) console.log(err.message)
                                            else{
                                              console.log('sortfile name append in filenames.txt')

                                              fs.readFile("filenames.txt",'utf-8',(err,data)=>{
                                                if(err) console.log(err.message)
                                                else{
                                                  const arr = data.split('\n')
                                                  arr.forEach(file => {
                                                    fs.rm(file,(err)=>{
                                                      if(err) console.log(err.message)
                                                      else{
                                                        console.log('files in filenames are deleted..!!  ')
                                                      }
                                                    })
                                                  })
                                                }
                                              })
                                            }
                                          })
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    }
  });
};

module.exports = fsFile;
